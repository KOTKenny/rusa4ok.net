﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rusa4ok.NET;
using Rusa4ok.NET.Models;

namespace Rusa4ok.NET.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class GalleryController : Controller
    {
        private EFContext db = new EFContext();

        // GET: Admin/Gallery
        public ActionResult Index()
        {
            return View(db.GalleryModels.ToList());
        }

        // GET: Admin/Gallery/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title, ImgName")] GalleryModels galleryModels, HttpPostedFileBase upload)
        {

            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                upload.SaveAs(Server.MapPath("~/images/gallery/" + fileName));

                galleryModels.ImgName = fileName;
            }

            if (ModelState.IsValid)
            {
                db.GalleryModels.Add(galleryModels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(galleryModels);
        }

        // GET: Admin/Gallery/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GalleryModels galleryModels = db.GalleryModels.Find(id);
            if (galleryModels == null)
            {
                return HttpNotFound();
            }
            return View(galleryModels);
        }

        // POST: Admin/Gallery/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Title, ImgName")] GalleryModels galleryModels, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                string fileName = System.IO.Path.GetFileName(upload.FileName);
                upload.SaveAs(Server.MapPath("~/images/gallery/" + fileName));

                galleryModels.ImgName = fileName;
                db.Entry(galleryModels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(galleryModels);
        }

        // GET: Admin/Gallery/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GalleryModels galleryModels = db.GalleryModels.Find(id);
            if (galleryModels == null)
            {
                return HttpNotFound();
            }
            return View(galleryModels);
        }

        // POST: Admin/Gallery/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GalleryModels galleryModels = db.GalleryModels.Find(id);
            db.GalleryModels.Remove(galleryModels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
