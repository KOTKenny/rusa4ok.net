﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Rusa4ok.NET.Models.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rusa4ok.NET.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private ApplicationRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public ActionResult Index()
        {
            var users = UserManager.Users;
            if (users != null)
            {
                List<DetailUserModel> listOfUsers = new List<DetailUserModel>();
                foreach (var user in users)
                {
                    listOfUsers.Add(new DetailUserModel { Id = user.Id, Email = user.Email, Roles = UserManager.GetRoles(user.Id) });
                }
                return View(listOfUsers);
            }
            return View();
        }

        public async Task<ActionResult> Edit(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user != null)
            {
                ViewBag.Roles = RoleManager.Roles;
                return View(new EditUserModel { Id = user.Id, Email = user.Email, Roles = UserManager.GetRoles(user.Id) });
            }
            return RedirectToAction("Login");
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditUserModel model, FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                var selectedIds = collection.GetValues("IdsToAdd");
                ApplicationUser user = await UserManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    //IdentityResult result = await UserManager.UpdateAsync(user);
                    IList<string> roles = UserManager.GetRoles(user.Id);
                    UserManager.RemoveFromRoles(user.Id, roles.ToArray());

                    IdentityResult result = await UserManager.AddToRolesAsync(user.Id, selectedIds);

                    if (result.Succeeded)
                    {
                        if (!string.IsNullOrEmpty(model.NewPassword))
                        {
                            user.PasswordHash = UserManager.PasswordHasher.HashPassword(model.NewPassword);
                            result = await UserManager.UpdateAsync(user);
                            if (result.Succeeded)
                            {
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                ModelState.AddModelError("", "Что-то пошло не так");
                            }
                        }

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Что-то пошло не так");
                    }
                }
            }
            return View(model);
        }
    }
}