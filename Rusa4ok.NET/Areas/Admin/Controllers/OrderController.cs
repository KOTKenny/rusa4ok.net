﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rusa4ok.NET;
using Rusa4ok.NET.Models;

namespace Rusa4ok.NET.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class OrderController : Controller
    {
        private EFContext db = new EFContext();

        public ActionResult Index()
        {
            return View(db.Order.ToList());
        }

        public ActionResult Create()
        {
            var ListOfCategories = new SelectList(db.Category.ToList(), "ID", "Name");
            ViewBag.Categories = ListOfCategories;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, CategoryId, Description, ImgName")] Order order, HttpPostedFileBase upload)
        {

            if (upload != null)
            {
                string fileName = System.IO.Path.GetFileName(upload.FileName);
                upload.SaveAs(Server.MapPath("~/images/orders/" + fileName));

                order.ImgName = fileName;
            }

            if (ModelState.IsValid)
            {
                order.CreateDate = DateTime.Now;
                db.Order.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            var ListOfCategories = new SelectList(db.Category.ToList(), "ID", "Name", order.CategoryId);
            ViewBag.Categories = ListOfCategories;
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Name, Description, ImgName")] Order order, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                string fileName = System.IO.Path.GetFileName(upload.FileName);
                upload.SaveAs(Server.MapPath("~/images/orders/" + fileName));

                order.ImgName = fileName;
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Order.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            var ListOfCategories = new SelectList(db.Category.ToList(), "ID", "Name", order.CategoryId);
            ViewBag.Categories = ListOfCategories;
            return View(order);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Order.Find(id);
            db.Order.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
