﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rusa4ok.NET;
using Rusa4ok.NET.Models;

namespace Rusa4ok.NET.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BackendController : Controller
    {
        private EFContext db = new EFContext();

        public ActionResult Index()
        {
            return View(db.Backend.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Value, Description, Tag")] Backend backend, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                db.Backend.Add(backend);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(backend);
        }

        public ActionResult Edit(string tag)
        {
            if (tag is null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Backend backend = db.Backend.Find(tag);
            if (backend == null)
            {
                return HttpNotFound();
            }
            return View(backend);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Value, Description, Tag")] Backend backend, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                db.Entry(backend).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(backend);
        }

        public ActionResult Delete(string tag)
        {
            if (tag is null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Backend backend = db.Backend.Find(tag);
            if (backend == null)
            {
                return HttpNotFound();
            }
            return View(backend);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string tag)
        {
            Backend backend = db.Backend.Find(tag);
            db.Backend.Remove(backend);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
