﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Rusa4ok.NET.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Rusa4ok.NET.Controllers.AdminController
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {

        private ApplicationRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public async Task<ActionResult> Index()
        {
            ApplicationUser user = await UserManager.FindByIdAsync(HttpContext.User.Identity.GetUserId());
            if (user != null)
            {
                return View(new DetailUserModel { Email = user.Email, Roles = UserManager.GetRoles(user.Id) });
            }
            return View();
        }
    }
}