﻿using Rusa4ok.NET.Models;
using System.Collections.Generic;
using System.Linq;

namespace Rusa4ok.NET.BLL.Helpers
{
    public class GalleryHelper
    {
        public static List<GalleryModels> GetAllGalleryModels()
        {
            EFContext db = new EFContext();
            return db.GalleryModels.ToList();
        }
    }
}