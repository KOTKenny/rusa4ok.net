﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rusa4ok.NET.BLL.Helpers
{
    public class BackendHelper
    {
        public static string TagToValue(string tag)
        {
            EFContext db = new EFContext();
            var value = db.Backend.Find(tag) is null ? string.Empty : db.Backend.Find(tag).Value;
            return value;
        }
    }
}