﻿using Rusa4ok.NET.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Rusa4ok.NET.BLL.Helpers
{
    public class SalesHelper
    {
        public static List<Sale> GetSales()
        {
            EFContext db = new EFContext();
            return db.Sale.Take(3).ToList();
        }
    }
}