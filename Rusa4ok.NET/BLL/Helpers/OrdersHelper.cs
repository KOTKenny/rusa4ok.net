﻿using Rusa4ok.NET.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Rusa4ok.NET.BLL.Helpers
{
    public class OrdersHelper
    {
        public static List<Order> GetNewOrders()
        {
            EFContext db = new EFContext();
            Backend setting = db.Backend.Find("DaysOfNewStatus");
            int countOfDays = setting == null ? 10 : Convert.ToInt32(setting.Value);
            return db.Order.Where(x => DbFunctions.AddDays(x.CreateDate, -countOfDays) <= DateTime.Now).ToList();
        }
    }
}