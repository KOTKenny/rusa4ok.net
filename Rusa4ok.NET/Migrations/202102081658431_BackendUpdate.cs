namespace Rusa4ok.NET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BackendUpdate : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Backends");
            AddPrimaryKey("dbo.Backends", "Tag");
            DropColumn("dbo.Backends", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Backends", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Backends");
            AddPrimaryKey("dbo.Backends", "Id");
        }
    }
}
