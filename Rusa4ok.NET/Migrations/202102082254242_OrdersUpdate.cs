namespace Rusa4ok.NET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ImgName", c => c.String(nullable: false, maxLength: 128));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ImgName");
        }
    }
}
