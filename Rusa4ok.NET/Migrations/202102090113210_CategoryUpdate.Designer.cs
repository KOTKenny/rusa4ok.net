// <auto-generated />
namespace Rusa4ok.NET.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CategoryUpdate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CategoryUpdate));
        
        string IMigrationMetadata.Id
        {
            get { return "202102090113210_CategoryUpdate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
