namespace Rusa4ok.NET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BackendModelUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Backends", "Value", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Backends", "Value", c => c.String(nullable: false, maxLength: 128));
        }
    }
}
