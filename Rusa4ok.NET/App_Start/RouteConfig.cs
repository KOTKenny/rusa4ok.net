﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Rusa4ok.NET
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "IndexAdmin",
                url: "Admin/{action}",
                defaults: new { controller = "Admin", action = "Index" },
                constraints: new { controller = "^Admin.*" },
                namespaces: new[] { "Rusa4ok.NET.Controllers" }
            );

            routes.MapRoute(
                name: "Catalog",
                url: "Catalog/{categoryId}/{orderId}",
                defaults: new { controller = "Home", action = "Catalog", categoryId = UrlParameter.Optional, orderId = UrlParameter.Optional },
                namespaces: new[] { "Rusa4ok.NET.Controllers" }
            );

            routes.MapRoute(
                name: "Sales",
                url: "Sales/{id}",
                defaults: new { controller = "Home", action = "Sales", id = UrlParameter.Optional },
                namespaces: new[] { "Rusa4ok.NET.Controllers" }
            );

            routes.MapRoute(
                name: "Main",
                url: "{action}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new[] { "Rusa4ok.NET.Controllers" }
            );

        }
    }
}
