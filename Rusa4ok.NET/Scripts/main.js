AOS.init({ duration: 1000 });

// jQuery counterUp
$('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
});

$(document).ready(function() {
    $('.owl-gallery').owlCarousel({
        loop: true,
        autoplay: true,
        margin: 10,
        items: 1,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 0
            },
            600: {
                items: 2,
                stagePadding: 50
            },
            1000: {
                items: 3,
                stagePadding: 100
            }
        }
    });
});

$(document).ready(function() {
    $('.owl-recommended').owlCarousel({
        loop: true,
        autoplay: true,
        margin: 40,
        items: 1,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 20
            },
            600: {
                items: 2,
                stagePadding: 20
            },
            1000: {
                items: 3,
                stagePadding: 20
            }
        }
    });
});

$(document).ready(function() {
    $('.owl-share').owlCarousel({
        loop: true,
        autoplay: true,
        margin: 10,
        items: 1,
    });
});

var triggerTabList = [].slice.call(document.querySelectorAll('#v-pills-tab a'))
triggerTabList.forEach(function(triggerEl) {
    var tabTrigger = new bootstrap.Tab(triggerEl)

    triggerEl.addEventListener('click', function(event) {
        event.preventDefault()
        tabTrigger.show()

        $(this).parent().find('.collapse').each(function(i, elem) {
            $(elem).collapse('hide');
        });

    });

});