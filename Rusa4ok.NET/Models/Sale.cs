﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rusa4ok.NET.Models
{
    public class Sale
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Описание")]
        [StringLength(3000)]
        public string Description { get; set; }

        public DateTime CreateDate { get; set; }
    }
}