﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Rusa4ok.NET.Models.Identity
{
    public class DetailUserModel
    {
        [Key]
        public string Id { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Роль")]
        public IList<string> Roles { get; set; }

    }
}