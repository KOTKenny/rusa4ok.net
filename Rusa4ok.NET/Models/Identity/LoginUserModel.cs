﻿using System.ComponentModel.DataAnnotations;

namespace Rusa4ok.NET.Models.Identity
{
    public class LoginUserModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}