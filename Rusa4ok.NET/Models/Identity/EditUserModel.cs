﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Rusa4ok.NET.Models.Identity
{
    public class EditUserModel
    {
        [Required]
        public string Id { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [Display(Name = "Роли")]
        public IList<string> Roles { get; set; }

    }
}