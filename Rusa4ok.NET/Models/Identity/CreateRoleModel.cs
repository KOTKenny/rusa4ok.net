﻿namespace Rusa4ok.NET.Models.Identity
{
    public class CreateRoleModel
    {
        public string Name { get; set; }
    }
}