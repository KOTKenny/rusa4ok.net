﻿namespace Rusa4ok.NET.Models.Identity
{
    public class EditRoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}