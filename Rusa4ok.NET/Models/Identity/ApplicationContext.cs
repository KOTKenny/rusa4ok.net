﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Configuration;

namespace Rusa4ok.NET.Models.Identity
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base(ConfigurationManager.AppSettings["ConnectionName"]) { }

        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }

        public System.Data.Entity.DbSet<ApplicationRole> IdentityRoles { get; set; }

        public System.Data.Entity.DbSet<Rusa4ok.NET.Models.Identity.DetailUserModel> DetailUserModels { get; set; }
    }
}