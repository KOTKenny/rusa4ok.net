﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Rusa4ok.NET.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
        }
    }
}