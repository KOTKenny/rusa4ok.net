using System.ComponentModel.DataAnnotations;

namespace Rusa4ok.NET.Models
{
    public partial class GalleryModels
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название")]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(128)]
        public string ImgName { get; set; }
    }
}
