﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rusa4ok.NET.Models
{
    public partial class Order
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Название")]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required]
        [StringLength(128)]
        public string ImgName { get; set; }

        public decimal Cost { get; set; }

        public DateTime? CreateDate { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }
    }

    public partial class Category
    {
        public int Id { get; set; }

        [Display(Name = "Название")]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(128)]
        public string ImgName { get; set; }

        public virtual List<Order> Orders { get; set; }
    }
}