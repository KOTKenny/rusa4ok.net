using System.ComponentModel.DataAnnotations;

namespace Rusa4ok.NET.Models
{
    public partial class Backend
    {
        [Key]
        [Display(Name = "���")]
        [Required]
        [StringLength(128)]
        public string Tag { get; set; }

        [Display(Name = "��������")]
        [Required]
        [StringLength(3000)]
        public string Value { get; set; }

        [Display(Name = "��������")]
        [Required]
        public string Description { get; set; }
    }
}
