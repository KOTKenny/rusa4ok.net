using System.Configuration;
using System.Data.Entity;

namespace Rusa4ok.NET
{

    public partial class EFContext : DbContext
    {
        public EFContext()
            : base(ConfigurationManager.AppSettings["ConnectionName"])
        {
        }

        public DbSet<Models.GalleryModels> GalleryModels { get; set; }

        public DbSet<Models.Backend> Backend { get; set; }

        public DbSet<Models.Order> Order { get; set; }

        public DbSet<Models.Category> Category { get; set; }

        public DbSet<Models.Sale> Sale { get; set; }
    }
}
